require_relative 'conflict_matrix_module'
require_relative 'subject'
require_relative 'scraper_module'

class Schedule
  attr_accessor :student_info, :subjects

  @@my_subs = { 'L' => 'Lunes', 'M' => 'Martes', 'I' => 'Miercoles', 'J' => 'Jueves', 'V' => 'Viernes', 'S' => 'Sabado', 'D' => 'Domingo' }

  @@schedules = {}

  def initialize(student_name_and_cod, subjects)
    @student_info = student_name_and_cod
    @subjects = subjects
  end

  #Get the conflict_matrix and the schedules
  def self.fetch_schedule
    robot = ScraperModule.login_pomelo(ScraperModule.url, ScraperModule.params)
    subject = []
    page = robot.get("https://pomelo.uninorte.edu.co/pls/prod/bwskfshd.P_CrseSchdDetl")
    form = page.forms[1]
    date = Date.today.to_s.split("-")
    year = date[0]
    month = date[1]
    semestre = (month.to_i <= 6) ? "Primer" : "Segundo"
    form.fields[0].options.each do |option|
      if (option.text.include? year)
        option.click if option.text.include? semestre
      end
    end
    # p form.fields[0].options[2].text
    page = form.submit
    day_and_time = {}
    subject_text = ""
    subject_name = ""
    subject_cod = ""
    subject_info = ""
    subject_nrc = ""
    subject_teacher = ""
    subject_credits = ""
    page = page.parser
    student_info = page.css(".staticheaders").text.split("\n")[1]
    table_horary = page.css(".datadisplaytable")
    table_horary.each_with_index do |tabla,i|
      i = i + 1
      if i%2 == 0
        tabla.css("tr").each_with_index do |info,k|
          if k>0
           info_day_and_time = info.css(".dddefault")
           day = @@my_subs[info_day_and_time[2].text]
           time = info_day_and_time[1].text
           day_and_time[:"#{day}"] = time
           time = time.split("-")
           begin_time = time[0].strip
           end_time = time[1].strip
           end_time = (end_time.include? "PM") ? ConflictMatrix.change_time_to_military(end_time) : end_time
           begin_time = (begin_time.include? "PM") ? ConflictMatrix.change_time_to_military(begin_time) : begin_time
           ConflictMatrix.matrix[day][begin_time] = "1"
           if end_time.include? ":28"
             end_time_changed = ConflictMatrix.ocupated_time(end_time)
             ConflictMatrix.matrix[day][end_time_changed] = "1"
           end
         end
        end
        day_and_time = {}
        subject.push(Subject.new(subject_cod,subject_nrc,subject_name,day_and_time,subject_credits,subject_teacher))
      else
        subject_text = tabla.css(".captiontext").text.split("-")
        subject_name = subject_text[0]
        subject_cod = subject_text[1].strip
        subject_info = tabla.css(".dddefault")
        subject_nrc = subject_info[1].text
        subject_teacher = subject_info[3].text
        subject_credits = subject_info[5].text.strip
      end
    end
    @@schedules[:"#{student_info}"] = Schedule.new(student_info,subject)
    array = ConflictMatrix.conflict_matrix_to_a_array(ConflictMatrix.matrix)
  end

  #Return schedules
  def self.schedules
    return @@schedules
  end


end
