class Subject

  attr_accessor :cod, :nrc, :name, :day_and_time, :credits, :teacher

  def initialize(cod,nrc,name,day_and_time,credits,teacher)
    @cod = cod
    @nrc = nrc
    @name = name
    @day_and_time = day_and_time
    @credits = credits
    @teacher = teacher
  end


end
