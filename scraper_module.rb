require 'mechanize'
require_relative 'console_module'

module ScraperModule
  @@url=  "https://pomelo.uninorte.edu.co/pls/prod/twbkwbis.P_ValLogin"

  @@params = {}

  def self.create_bot
    return Mechanize.new
  end

  def self.params
   return @@params
  end
  def self.url
   return @@url
  end

  #For access to pomelo
  def self.login_pomelo(url, params)
    robot = create_bot
    access = login?(robot)
    unless (access)
     ConsoleModule.get_info
     robot = ScraperModule.login_pomelo(ScraperModule.url, ScraperModule.params)
    end
    return robot
  end

  #Return a boolean proving that you enter the correct user and password
  def self.login?(robot)
    page = robot.get url
    form = page.forms[0]
    form.sid = ScraperModule.params["sid"]
    form.PIN = ScraperModule.params["PIN"]
    access = form.submit.parser.css(".plaintable")
    access = access.size == 0
    return access
  end

  #Get the html of a page
  def self.fetch_html(url)
    bot = create_bot
    page = bot.get(url)
  end
end
