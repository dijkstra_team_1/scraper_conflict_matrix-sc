=begin
Ruby 2.4.1
gem mechanize (2.7.5)
gem console_view_helper (0.0.4)
=end
require_relative 'scraper_module'
require_relative 'conflict_matrix_module'
require_relative 'console_module'
require_relative 'schedule'

ConflictMatrix.initialize_conflic_matrix
puts "Bienvenido al realizador de matrices de conflictos, INGRESE ENTER PARA CONTINUAR"
add = gets.chomp
until (add == "0") do
  ConsoleModule.get_info
  array_conflict_matrix = Schedule.fetch_schedule
  puts "Desea seguir ingresando usuarios (0 = No) O PULSE ENTER PARA CONTINUAR"
  add = gets.chomp
end
ConsoleModule.show_table(array_conflict_matrix)
