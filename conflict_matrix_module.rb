module ConflictMatrix

    @@matrix = {
      'Lunes' => {},
      'Martes' => {},
      'Miercoles' => {},
      'Jueves' => {},
      'Viernes' => {},
      'Sabado' => {},
      'Domingo' => {}
    }
     #Allows change our normal time like 2:00pm to military 14:00pm
     def self.change_time_to_military(string)
       change_time = string.split(":")
       change_time[0] = (change_time[0].to_i + 12).to_s
       string = (string.include? "12") ?  string : change_time.join(":")
     end

     #This method gets the last ocupated hour of your class
     def self.ocupated_time(time)
       end_time_split = time.split(":")
       end_time_split[0] = (end_time_split[0].to_i - 1).to_s
       end_time_split_split = end_time_split[1].split(" ")
       end_time_split_split[0] = (end_time_split_split[0].to_i + 2).to_s
       end_time_split_split[1] = (time.include? "12") ? "AM" : end_time_split_split[1]
       end_time_split[1] = end_time_split_split.join(" ")
       end_time = end_time_split.join(":")
     end

      #for transform the conflict_matrix hash in an array
     def self.conflict_matrix_to_a_array(conflict_matrix)
        array_conflict_matrix = []
        conflict_matrix.each_with_index do |(key,value),i|
        array_conflict_matrix.push([key])
        value.each do |key2,value2|
        (value2.include? "1") ? array_conflict_matrix[i].push(key2) : array_conflict_matrix[i].push("Libre")
        end
     end
      array_conflict_matrix
     end
      #this function fill out our conflict_matrix hash with 0
     def self.initialize_conflic_matrix
        @@matrix.each do |key,value|
         (6..19).each { |n| n>11 ? value["#{n}:30 PM"] = "0" : value["#{n}:30 AM"] = "0"  }
        end
        # pp @@matrix
     end
      #Return the conflict_matrix
     def self.matrix
       return @@matrix
     end

end
