require_relative 'scraper_module'
require 'console_view_helper'

module ConsoleModule
 #For get user information
def self.get_info
  puts "Ingrese su usuario uninorte"
  user = ConsoleViewHelper.input
  ScraperModule.params["sid"] = user
  puts "Ingrese su contraseña"
  pass = ConsoleViewHelper.hidden_input
  ScraperModule.params["PIN"] = pass
  puts ""
end
 #Allows show an array in a table form
def self.show_table(array)
  puts  ConsoleViewHelper.table(array)
end

end
